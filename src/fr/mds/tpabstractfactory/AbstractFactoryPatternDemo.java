package fr.mds.tpabstractfactory;

import fr.mds.tpabstractfactory.color.Color;
import fr.mds.tpabstractfactory.factory.AbstractFactory;
import fr.mds.tpabstractfactory.factory.FactoryProducer;
import fr.mds.tpabstractfactory.item.CombinedItem;
import fr.mds.tpabstractfactory.item.DrawItem;
import fr.mds.tpabstractfactory.item.Item;
import fr.mds.tpabstractfactory.shape.Shape;

public class AbstractFactoryPatternDemo {

	public static void main(String[] args) {
		// --- EXO 1 --- //
		System.out.println("Exo 1");
		// get shape factory
		AbstractFactory shapeFactory = FactoryProducer.getFactory(AllObjects.SHAPE);
		
		// get an object of Shape Circle
		Shape shape1 = shapeFactory.getShape(AllObjects.CIRCLE);
		
		// call draw method of Shape Circle
		shape1.draw();
		
		// get an object of Shape Rectangle
		Shape shape2 = shapeFactory.getShape(AllObjects.RECTANGLE);
		
		// call draw method of Shape Rectangle
		shape2.draw();
		
		// get an object of Shape Square
		Shape shape3 = shapeFactory.getShape(AllObjects.SQUARE);
		
		// call draw method of Shape Square
		shape3.draw();
		
		// get color factory
		AbstractFactory colorFactory = FactoryProducer.getFactory(AllObjects.COLOR);
		
		// get an object of Color Red
		Color color1 = colorFactory.getColor(AllObjects.RED);
		
		// call fill method of Red
		color1.fill();
		
		// get an object of Color Green
		Color color2 = colorFactory.getColor(AllObjects.GREEN);
		
		// call fill method of Green
		color2.fill();
		
		// get an object of Color Blue
		Color color3 = colorFactory.getColor(AllObjects.BLUE);
		
		// call fill method of Color Blue
		color3.fill();
		
		// get an object of Shape Circle
		Shape myShape = FactoryProducer.getFactory(AllObjects.SHAPE).getShape(AllObjects.CIRCLE);
		
		// call draw method of Shape Circle
		myShape.draw();

		System.out.println();
		
		// --- EXO 2 --- //
		System.out.println("Exo 2");
		// get directly an item with factory auto selecting
		Item item = FactoryProducer.getItem(AllObjects.BLUE);
		System.out.println("this is a " + item.getName());
		
		item = FactoryProducer.getItem(AllObjects.GREEN);
		System.out.println("this is a " + item.getName());
		
		item = FactoryProducer.getItem(AllObjects.RED);
		System.out.println("this is a " + item.getName());
		
		item = FactoryProducer.getItem(AllObjects.SQUARE);
		System.out.println("this is a " + item.getName());
		
		item = FactoryProducer.getItem(AllObjects.RECTANGLE);
		System.out.println("this is a " + item.getName());
		
		item = FactoryProducer.getItem(AllObjects.CIRCLE);
		System.out.println("this is a " + item.getName());

		System.out.println();
		
		// --- EXO 3 --- //
		System.out.println("Exo 3");
		///////////////////////////////////////////////////
		CombinedItem cItem1 = new CombinedItem();
		cItem1.setColor(colorFactory.getColor(AllObjects.BLUE));
		cItem1.setShape(shapeFactory.getShape(AllObjects.SQUARE));
		
		CombinedItem cItem2 = new CombinedItem();
		cItem2.setColor(colorFactory.getColor(AllObjects.RED));
		cItem2.setShape(shapeFactory.getShape(AllObjects.SQUARE));
		
		CombinedItem cItem3 = new CombinedItem();
		cItem3.setColor(colorFactory.getColor(AllObjects.BLUE));
		cItem3.setShape(shapeFactory.getShape(AllObjects.CIRCLE));
		
		CombinedItem cItem4 = new CombinedItem();
		cItem4.setColor(colorFactory.getColor(AllObjects.GREEN));
		cItem4.setShape(shapeFactory.getShape(AllObjects.RECTANGLE));
		
		DrawItem dItem = new DrawItem();
		dItem.setName("mon dessin");
		dItem.getItems().add(cItem1);
		dItem.getItems().add(cItem2);
		dItem.getItems().add(cItem3);
		dItem.getItems().add(cItem4);
		dItem.print();

	}
}
