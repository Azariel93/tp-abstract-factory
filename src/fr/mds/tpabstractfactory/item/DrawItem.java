package fr.mds.tpabstractfactory.item;

import java.util.ArrayList;
import java.util.List;

public class DrawItem {
	
	private String name;
	private List<CombinedItem> itemList = new ArrayList<CombinedItem>();

	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}

	public List<CombinedItem> getItems() {
		return this.itemList;
	}

	public void print() {
		System.out.println("In this drawing, there is :");
		for (CombinedItem combinedItem : itemList) {
			System.out.println("  - a " + combinedItem.getColor() + " " + combinedItem.getShape());
		}
	}

}
