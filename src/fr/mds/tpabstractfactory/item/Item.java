package fr.mds.tpabstractfactory.item;

public interface Item {

	String getName();
	
}
