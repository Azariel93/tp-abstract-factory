package fr.mds.tpabstractfactory.item;

import fr.mds.tpabstractfactory.color.Color;
import fr.mds.tpabstractfactory.shape.Shape;

public class CombinedItem {

	private Color color;
	private Shape shape;
	
	public void setColor(Color color) {
		this.color = color;
	}
	
	public Color getColor() {
		return this.color;
	}

	public void setShape(Shape shape) {
		this.shape = shape;
	}
	
	public Shape getShape() {
		return this.shape;
	}

}
