package fr.mds.tpabstractfactory.factory;

import fr.mds.tpabstractfactory.AllObjects;
import fr.mds.tpabstractfactory.color.Blue;
import fr.mds.tpabstractfactory.color.Color;
import fr.mds.tpabstractfactory.color.Green;
import fr.mds.tpabstractfactory.color.Red;

public class ColorFactory extends AbstractFactory {

	@Override
	public Color getColor(String color) {
		Color temp;
		switch (color) {
		case AllObjects.BLUE:
			temp =  new Blue();
			break;
		case AllObjects.GREEN:
			temp =  new Green();
			break;
		case AllObjects.RED:
			temp =  new Red();
			break;
		default:
			System.out.println("Not supported");
			temp = null;
			break;
		}
		return temp;
	}

}
