package fr.mds.tpabstractfactory.factory;

import fr.mds.tpabstractfactory.AllObjects;
import fr.mds.tpabstractfactory.factory.*;
import fr.mds.tpabstractfactory.item.Item;

public class FactoryProducer {

	public static AbstractFactory getFactory(String factory) {
		AbstractFactory temp;
		switch (factory) {
		case AllObjects.SHAPE:
			temp = new ShapeFactory();
			break;
		case AllObjects.COLOR:
			temp = new ColorFactory();
			break;
		default:
			System.out.println("Not supported");
			temp = null;
			break;
		}
		return temp;
	}
	
	public static Item getItem(String item) {
		Item temp;
		switch (item) {
		case AllObjects.BLUE :
		case AllObjects.GREEN:
		case AllObjects.RED:
			temp = (Item) new ColorFactory().getColor(item);
			break;
		case AllObjects.CIRCLE:
		case AllObjects.RECTANGLE:
		case AllObjects.SQUARE:
			temp = (Item) new ShapeFactory().getShape(item);
			break;
		default:
			temp = null;
			break;
		}
		return temp;
	}
}
