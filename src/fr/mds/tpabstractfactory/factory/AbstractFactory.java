package fr.mds.tpabstractfactory.factory;

import fr.mds.tpabstractfactory.color.*;
import fr.mds.tpabstractfactory.shape.*;

public abstract class AbstractFactory {

	public Shape getShape(String shape) {
		return null;
	}
	
	public Color getColor(String color) {
		return null;
	}
	
}
