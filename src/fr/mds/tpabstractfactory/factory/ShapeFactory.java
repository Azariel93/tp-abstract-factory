package fr.mds.tpabstractfactory.factory;

import fr.mds.tpabstractfactory.AllObjects;
import fr.mds.tpabstractfactory.shape.*;

public class ShapeFactory extends AbstractFactory {

	@Override
	public Shape getShape(String shape) {
		Shape temp;
		switch (shape) {
		case AllObjects.CIRCLE:
			temp  = new Circle();
			break;
		case AllObjects.RECTANGLE:
			temp = new Rectangle();
			break;
		case AllObjects.SQUARE:
			temp = new Square();
			break;
		default:
			System.out.println("Not supported");
			temp = null;
			break;
		}
		return temp;
	}
}
