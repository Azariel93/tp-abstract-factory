package fr.mds.tpabstractfactory.color;

public interface Color {

	public abstract void fill();
	
}
