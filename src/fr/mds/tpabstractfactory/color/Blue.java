package fr.mds.tpabstractfactory.color;

import fr.mds.tpabstractfactory.AllObjects;
import fr.mds.tpabstractfactory.item.Item;

public class Blue implements Color, Item {

	@Override
	public void fill() {
		System.out.println("Nice, it's a Blue");
	}

	@Override
	public String getName() {
		return AllObjects.BLUE;
	}
	
	@Override
	public String toString() {
		return AllObjects.BLUE;
	}

}
