package fr.mds.tpabstractfactory.color;

import fr.mds.tpabstractfactory.AllObjects;
import fr.mds.tpabstractfactory.item.Item;

public class Red implements Color, Item {

	@Override
	public void fill() {
		System.out.println("Nice, it's a Red");
	}

	@Override
	public String getName() {
		return AllObjects.RED;
	}
	
	@Override
	public String toString() {
		return AllObjects.RED;
	}

}
