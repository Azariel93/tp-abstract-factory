package fr.mds.tpabstractfactory.shape;

import fr.mds.tpabstractfactory.AllObjects;
import fr.mds.tpabstractfactory.item.Item;

public class Square implements Shape, Item {

	@Override
	public void draw() {
		System.out.println("Wow, it's a Square");
	}

	@Override
	public String getName() {
		return AllObjects.SQUARE;
	}
	
	@Override
	public String toString() {
		return AllObjects.SQUARE;
	}

}
