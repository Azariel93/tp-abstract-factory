package fr.mds.tpabstractfactory.shape;

import fr.mds.tpabstractfactory.AllObjects;
import fr.mds.tpabstractfactory.item.Item;

public class Circle implements Shape, Item {

	@Override
	public void draw() {
		System.out.println("Wow, it's a Circle");
	}

	@Override
	public String getName() {
		return AllObjects.CIRCLE;
	}
	
	@Override
	public String toString() {
		return AllObjects.CIRCLE;
	}

}
