package fr.mds.tpabstractfactory.shape;

import fr.mds.tpabstractfactory.AllObjects;
import fr.mds.tpabstractfactory.item.Item;

public class Rectangle implements Shape, Item {

	@Override
	public void draw() {
		System.out.println("Wow, it's a Rectangle");
	}

	@Override
	public String getName() {
		return AllObjects.RECTANGLE;
	}
	
	@Override
	public String toString() {
		return AllObjects.RECTANGLE;
	}

}
