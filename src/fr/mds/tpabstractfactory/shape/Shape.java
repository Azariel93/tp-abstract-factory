package fr.mds.tpabstractfactory.shape;

public interface Shape {

	public abstract void draw();
	
}
